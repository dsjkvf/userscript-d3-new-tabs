// ==UserScript==
// @name        d3 - open links in new tabs
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-d3-new-tabs
// @downloadURL https://bitbucket.org/dsjkvf/userscript-d3-new-tabs/raw/master/d3nt.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-d3-new-tabs/raw/master/d3nt.user.js
// @match       *://*.d3.ru/*
// @require     https://gist.github.com/raw/2625891/waitForKeyElements.js
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @run-at      document-start
// @version     1.0.3
// ==/UserScript==

function linksInNewTabs() {
    var links = document.getElementsByTagName('a');
    for (var i = 0; i < links.length; i++) {
        if (links[i].href.indexOf('javascript') < 0) {
            links[i].target = '_blank';
        };
    };
};
waitForKeyElements("a.b-link", linksInNewTabs );
